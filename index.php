<?php	
	include_once("connect.php");
	
	if(!empty($_POST["salvar"])){
		if(empty($_POST["id"])){
			$email = $_POST["email"];
			$password = $_POST["password"];		
			$sql_insert = "INSERT INTO user (email,password) VALUES (:email,:password)";
			$query = $conn->prepare($sql_insert);
			$query->execute(array(":email"=>$email,":password"=>$password));
			$id = $conn->lastInsertId();
			header("location:?id=".$id);
		}else{
			$id = $_POST["id"];
			$email = $_POST["email"];
			$password = $_POST["password"];		
			$sql_update = "Update user set email = :email, password = :password where id = :id";
			$query = $conn->prepare($sql_update);
			$query->bindParam(":id", $_POST["id"], PDO::PARAM_INT);
			$query->bindParam(":email", $_POST["email"], PDO::PARAM_STR);
			$query->bindParam(":password", $_POST["password"], PDO::PARAM_STR);
			$query->execute();
		}
	}elseif(!empty($_POST["remover"])){
		$sql_remover = "DELETE FROM user WHERE id = :id";
		$query = $conn->prepare($sql_remover);
		$query->bindParam(':id', $_POST['remover'], PDO::PARAM_INT);
		$query->execute();
		header("location:index.php");
	}
	if(!empty($_GET)){
		$sql_user = "select id, email, password from user where id = ".$_GET["id"]; 
		$res_edit_user = $conn->query($sql_user);
		
		$dados_user_edit = $res_edit_user->fetchObject();
		$id = $dados_user_edit->id;
		$email = $dados_user_edit->email;
		$password = $dados_user_edit->password;
	}
	$sql_list = "select id, email from user order by id";
	$res = $conn->query($sql_list);
?>
<?php include_once("header.html"); ?>
<div class="container">
	<form class="form-signin" method='post'>
		<h2 class="form-signin-heading"><?php echo (empty($_GET["id"]))?"Cadastre-se":"Editar"; ?></h2>
		<?php if(!empty($_GET["id"])):?>
			<input value="<?php echo (!empty($dados_user_edit->id))?$dados_user_edit->id:""; ?>" name="id" type="hidden" >
		<?php endif;?>
		<input value="<?php echo (!empty($dados_user_edit->email))?$dados_user_edit->email:""; ?>" name="email" type="email" class="input-block-level" required placeholder="Email address">
		<input value="<?php echo (!empty($dados_user_edit->password))?$dados_user_edit->password:""; ?>" name="password" type="password" class="input-block-level" required placeholder="Password">
		<button name="salvar" value="salvar" class="btn btn-large btn-primary" type="submit">Salvar</button>
		<button name="remover" value="<?php echo (!empty($dados_user_edit->id))?$dados_user_edit->id:""; ?>" onclick="confirm('Tem certeza que deseja excluir este usu�rio?');" <?php echo (empty($_GET["id"]))?"disabled=true":""; ?> class="btn btn-large btn-primary" type="submit">Remover</button>
		<button onclick="window.location='index.php'" class="btn btn-large btn-primary" type="reset">Incluir Novo</button>
	</form>
		
	<?php if($res->rowCount() > 0):?>
	<form class="form-signin" method='post'>
	<h3><center>Usu�rios Cadastrados</center></h3>
	<table class="table table-hover tabela-cadastrados">
		<thead>
			<tr>
				<th>ID</th>
				<th>Email</th>
				<th>Ac�o</th>
			</tr>
		</thead>
		<tbody>
			<?php foreach ($conn->query($sql_list) as $row):?>
			<tr>
				<th><?php echo $row["id"]?></th>
				<td><?php echo $row["email"]?></td>
				<td>
					<button onclick="window.location='?id=<?php echo $row["id"];?>'" name="editar" value="editar" class="btn btn-primary" type="button">Editar</button>
					<button name="remover" value="<?php echo $row["id"]; ?>" onclick="confirm('Tem certeza que deseja excluir este usu�rio?');" class="btn btn-primary" type="submit">Remover</button>
				</td>
			</tr>
			<?php endforeach;?>
		</tbody>
	</table>
	</form>
	<?php endif;?>
</div>
<?include_once("footer.html");?>